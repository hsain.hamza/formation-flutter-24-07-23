import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

/// [Translations]
///
/// - Wrapper for locale
/// - Initialize [_localizedValues] with load method and [Locale]
///
class Translations {
  static Map<dynamic, dynamic>? _localizedValues;

  Locale locale;

  Translations(this.locale) {
    _localizedValues = null;
  }

  get currentLanguage => locale.languageCode;

  String text(
    String key, {
    Map<String, dynamic>? values,
  }) {
    var output = '-$key-';

    if(_localizedValues != null){
      output = _localizedValues![key] ?? '-$key-';
    }
    if (values == null) {
      return output;
    } else {
      values.forEach((String key, dynamic value) {
        if (value != null && (value is String || value is num)) {
          output = output.replaceAll('{{$key}}', value.toString());
        }
      });
      return output;
    }
  }

  /// Load the locale Json file with [locale]
  /// and return a [Transactions] with
  static Future<Translations> load(Locale locale) async {
    Translations translations = Translations(locale);
    String jsonContent =
        await rootBundle.loadString("locales/i18n_${locale.languageCode}.json");
    _localizedValues = json.decode(jsonContent);

    return translations;
  }

  static Translations? of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }
}
