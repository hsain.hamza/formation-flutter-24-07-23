import 'package:dio/dio.dart';

/// [HttpDioHelper] is singleton and provide Dio http client configure with [BaseOptions]
class HttpDioHelper {
  HttpDioHelper._();

  static final HttpDioHelper _instance = HttpDioHelper._();

  /// Get the [Dio] client http
  static Dio get clientHttp => _instance._client;

  /// Set the [Dio] client and provides it for all the application or test
  static set clientHttp(Dio dio) {
    _instance._client = dio;
  }

  Dio _client = Dio();
}
