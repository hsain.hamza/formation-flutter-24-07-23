import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:profile_app/app/modules/contracts/bloc/contracts_bloc.dart';
import 'package:profile_app/app/modules/profile/bloc/login/login_bloc.dart';
import 'package:profile_app/app/modules/profile/bloc/profile/profile_bloc.dart';
import 'package:profile_app/app/modules/profile/bloc/user_info/user_info_bloc.dart';
import 'package:profile_app/app/screens/contracts/contracts_screen.dart';
import 'package:profile_app/app/screens/image_screen.dart';
import 'package:profile_app/app/screens/profil/profile_screen.dart';
import 'package:profile_app/app/screens/user/user_screen.dart';

const String kInitialRoute = kUserRoute;

const String kUserRoute = '/user';
const String kImageRoute = '/image';
const String kProfileRoute = '/profile';
const String kContractsRoute = '/contracts';

final Map<String, WidgetBuilder> kRoutes = {
  kUserRoute: (context) => BlocProvider<UserInfoBloc>(
        create: (_) => UserInfoBloc(),
        child: const UserScreen(),
      ),
  kContractsRoute: (context) => BlocProvider<ContractsBloc>(
        create: (_) => ContractsBloc(),
        child: const ContractsScreen(),
      ),
  kImageRoute: (context) => MultiBlocProvider(
        providers: [
          BlocProvider<LoginBloc>(
            create: (_) => LoginBloc(),
          ),
        ],
        child: const ImageScreen(),
      ),
  kProfileRoute: (context) => BlocProvider<ProfileBloc>(
      create: (_) => ProfileBloc(), child: const ProfileScreen()),
};
