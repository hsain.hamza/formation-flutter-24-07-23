import 'package:get_it/get_it.dart';
import 'package:profile_app/app/modules/contracts/data/local/contracts_local_provider.dart';
import 'package:profile_app/app/modules/contracts/data/remote/contracts_remote_provider.dart';
import 'package:profile_app/app/modules/contracts/repository/contracts_repository.dart';
import 'package:profile_app/app/modules/profile/data/local/profile_db_provider.dart';
import 'package:profile_app/app/modules/profile/data/local/profile_local_provider.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider_impl.dart';
import 'package:profile_app/app/modules/profile/repository/profile_repository.dart';

final locator = GetIt.instance;

void setupLocator(){
  locator.registerLazySingleton<ProfileLocalProvider>(() => ProfileDbProvider());
  locator.registerLazySingleton<ProfileRemoteProvider>(() => ProfileRemoteProviderImpl());
  locator.registerLazySingleton<ProfileRepository>(() => ProfileRepositoryImpl());

  locator.registerLazySingleton<ContractsRemoteProvider>(() => ContractsRemoteProviderImpl());
  locator.registerLazySingleton<ContractsLocalProvider>(() => ContractsDbProvider());
  locator.registerLazySingleton<ContractsRepository>(() => ContractsRepositoryImpl());

}