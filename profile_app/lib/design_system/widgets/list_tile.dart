import 'package:flutter/material.dart';

class ProListTile extends StatefulWidget {
  const ProListTile({Key? key}) : super(key: key);

  @override
  State<ProListTile> createState() => _ProListTileState();
}

class _ProListTileState extends State<ProListTile> {
  late int _counter;

  @override
  void initState() {
    _counter = 0;
    super.initState();
  }

  @override
  void dispose() {
    // liberer les resources
    // unsubscribe des flux de données
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "$_counter",
          style: const TextStyle(fontSize: 60),
        ),
        IconButton(
          onPressed: () {
            setState(() {
              _counter ++;
            });
          },
          icon: const Icon(Icons.add),
        ),
      ],
    );
  }
}
