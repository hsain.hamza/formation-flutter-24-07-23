import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:profile_app/app/screens/image_screen.dart';
import 'package:profile_app/app/screens/user/user_screen.dart';
import 'package:profile_app/core/constants/routes.dart';
import 'package:profile_app/core/i18n/translations_delegate.dart';

class ProfileApp extends StatelessWidget {
  const ProfileApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'GilroyRegular'),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        TranslationsDelegate(),
      ],
      supportedLocales: kSupportedLocales.map((e) => Locale(e)).toList(),
      localeResolutionCallback: _localResolutionCallback,
      initialRoute: kInitialRoute,
      routes: kRoutes,
    );
  }

  Locale? _localResolutionCallback(
    Locale? locale,
    Iterable<Locale> supportedLocales,
  ) {
    
    return const Locale('fr', 'FR');
  }
}
