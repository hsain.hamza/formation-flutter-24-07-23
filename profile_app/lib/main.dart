import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:profile_app/core/di/locator.dart';
import 'package:profile_app/core/http/http_dio_helper.dart';
import 'package:profile_app/profile_app.dart';

void main() {

  HttpDioHelper.clientHttp = Dio(
    BaseOptions(
      connectTimeout: const Duration(seconds: 10),
      baseUrl: 'https://demo0635484.mockable.io',
    ),
  );

  setupLocator();

  runApp(const ProfileApp());
}


