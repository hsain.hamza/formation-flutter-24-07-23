import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:profile_app/app/modules/profile/bloc/user_info/user_info_bloc.dart';
import 'package:profile_app/app/screens/user/widgets/user_footer.dart';
import 'package:profile_app/app/screens/user/widgets/user_headline.dart';
import 'package:profile_app/app/screens/user/widgets/user_image.dart';
import 'package:profile_app/app/screens/user/widgets/user_statistics.dart';
import 'package:profile_app/core/constants/routes.dart';
import 'package:profile_app/core/i18n/translations.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key}) : super(key: key);

  @override
  State<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen>
    with AfterLayoutMixin<UserScreen> {
  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    BlocProvider.of<UserInfoBloc>(context).add(UserInfoLoadingData());
  }

  @override
  Widget build(BuildContext context) {
    final trans = Translations.of(context)!;
    return Scaffold(
      backgroundColor: const Color(0xfff9fbfc),
      appBar: AppBar(
        title: Text(trans.text("core_error_label")),
      ),
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Expanded(
              flex: 2,
              child: BlocBuilder<UserInfoBloc, UserInfoState>(
                builder: (context, state) {
                  if (state is UserInfoSuccess) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed(kImageRoute,
                            arguments: "My awesome arg");
                      },
                      child: ProUserImage(
                        imageUrl: state.userInfo.imageUrl,
                      ),
                    );
                  }
                  if (state is UserInfoFailure) {
                    return const Center(
                      child: Icon(Icons.warning_amber),
                    );
                  }

                  return const Center(child: CircularProgressIndicator());
                },
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BlocBuilder<UserInfoBloc, UserInfoState>(
                      builder: (context, state) {
                        if (state is UserInfoSuccess) {
                          return Padding(
                            padding: const EdgeInsets.only(top: 40, left: 30),
                            child: ProUserHeadline(
                              line1: "Bonjour",
                              line2: state.userInfo.fullName,
                              line3: state.userInfo.email,
                            ),
                          );
                        }
                        if (state is UserInfoFailure) {
                          return const Center(child: Icon(Icons.warning_amber));
                        }
                        return const CircularProgressIndicator();
                      },
                    ),
                    const Expanded(child: ProUserStatistics()),
                    ProUserFooter(
                      onProfileClick: () {
                        Navigator.of(context).pushNamed(kProfileRoute);
                      },
                      onContractsClick: (){
                        Navigator.of(context).pushNamed(kContractsRoute);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
