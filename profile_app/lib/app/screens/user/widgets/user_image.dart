import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProUserImage extends StatelessWidget {
  final String? imageUrl;
  const ProUserImage({Key? key, this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return CachedNetworkImage(
      width: double.infinity,
      height: double.infinity,
      fit: BoxFit.cover,
      imageUrl: imageUrl ?? "",
      placeholder: (context, url) => const CircularProgressIndicator(),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }
}
