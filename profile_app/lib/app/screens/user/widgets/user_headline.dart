import 'package:flutter/material.dart';

class ProUserHeadline extends StatelessWidget {
  final String? line1;
  final String? line2;
  final String? line3;

  const ProUserHeadline({
    Key? key,
    this.line1,
    this.line2,
    this.line3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          line1 ?? "",
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          line2 ?? "",
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(
          height: 14,
        ),
        Text(
          line3 ?? "",
          style: const TextStyle(
            color: Color(0xff33394f),
            fontSize: 20,
          ),
        ),
      ],
    );
  }
}
