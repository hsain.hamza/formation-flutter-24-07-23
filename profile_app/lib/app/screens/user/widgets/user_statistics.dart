import 'package:flutter/material.dart';

class ProUserStatistics extends StatelessWidget {
  const ProUserStatistics({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> items = ["Applied", "Reviewd", "Contacted"];

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: items
          .map(
            (e) => ProStatisticItem(
              label: e,
              statistics: "30",
            ),
          )
          .toList(),
    );
  }
}

class ProStatisticItem extends StatelessWidget {
  final String? label;
  final String statistics;

  const ProStatisticItem({
    Key? key,
    required this.label,
    required this.statistics,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          label ?? 'N/A',
          style: const TextStyle(
            fontSize: 20,
            color: Colors.grey,
          ),
        ),
        Text(
          statistics,
          style: const TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
