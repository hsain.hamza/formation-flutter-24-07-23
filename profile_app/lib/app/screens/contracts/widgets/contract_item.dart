import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ContractItem extends StatelessWidget {
  final String? imageUrl;
  final String? title1;
  final String? title2;
  final String? title3;
  final DateTime? exipre;

  const ContractItem({
    Key? key,
    this.imageUrl,
    this.title1,
    this.title2,
    this.title3,
    this.exipre,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.all(18),
      width: double.infinity,
      height: 120,
      decoration: const BoxDecoration(
        color: Color(0xfffde9cc),
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 70,
            height: 70,
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl: imageUrl ?? "",
              placeholder: (context, url) => const Center(child:  CircularProgressIndicator()),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title1 ?? "",
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
                Text(
                  title2 ?? "",
                  style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  title3 ?? "",
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:  [
                    const Icon(Icons.lock_clock),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      "Expire le ${DateFormat.yMMMd().format(exipre ?? DateTime.now())}",
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
