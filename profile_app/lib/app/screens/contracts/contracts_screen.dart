import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:profile_app/app/modules/contracts/bloc/contracts_bloc.dart';
import 'package:profile_app/app/screens/contracts/widgets/contract_item.dart';

class ContractsScreen extends StatefulWidget {
  const ContractsScreen({Key? key}) : super(key: key);

  @override
  State<ContractsScreen> createState() => _ContractsScreenState();
}

class _ContractsScreenState extends State<ContractsScreen> with AfterLayoutMixin<ContractsScreen> {

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    BlocProvider.of<ContractsBloc>(context).add(LoadAllContracts());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ma liste des contrats"),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Mes contracts",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Text(
                "Consulter ma liste de contrats",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(child: BlocBuilder<ContractsBloc, ContractsState>(
                builder: (context, state) {
                  if (state is ContractsLoaded) {
                    return RefreshIndicator(
                      onRefresh: () async {
                        BlocProvider.of<ContractsBloc>(context).add(LoadAllContracts());
                      },
                      child: ListView.builder(
                        itemCount: state.contracts.length,
                        itemBuilder: (_, i) =>
                            ContractItem(
                              imageUrl: state.contracts[i].picture,
                              title1: state.contracts[i].holder,
                              title2: state.contracts[i].contractNumber,
                              title3: state.contracts[i].email,
                              exipre: state.contracts[i].expire,
                            ),
                      ),
                    );
                  }

                  if (state is ContractsError) {
                    return SizedBox(
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(Icons.warning_amber, size: 100, color: Colors.red,),
                          Text("Une erreur est survenue", style: TextStyle(fontSize: 20),),
                        ],
                      ),
                    );
                  }

                  return const Center(child: CircularProgressIndicator());
                },
              )),
            ],
          ),
        ),
      ),
    );
  }


}
