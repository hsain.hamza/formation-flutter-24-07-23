import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:profile_app/app/modules/profile/bloc/login/login_bloc.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider_impl.dart';
import 'package:profile_app/app/modules/profile/repository/profile_repository.dart';
import 'package:profile_app/core/di/locator.dart';
import 'package:profile_app/core/utils/validator_service.dart';

class ImageScreen extends StatefulWidget {
  const ImageScreen({Key? key}) : super(key: key);

  @override
  State<ImageScreen> createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {
  final _firstnameController = TextEditingController();
  final _lastnameController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  final ProfileRepository _profileRepository = locator.get();

  @override
  void dispose() {
    _firstnameController.removeListener(_nameListener);
    super.dispose();
  }

  @override
  void initState() {
    _firstnameController.addListener(_nameListener);
    super.initState();
  }

  _nameListener() {
    print("_firstnameController listner");
  }

  @override
  Widget build(BuildContext context) {
    final routeArguments =
        ModalRoute.of(context)?.settings.arguments as String?;

    return Container(
      color: Colors.white,
      child: SafeArea(
        top: true,
        bottom: false,
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Mon image"),
          ),
          body: Column(
            children: [
              Image.asset(
                "assets/images/user-image.png",
              ),
              Text("ROUTE ARGUMENT : $routeArguments"),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      FutureBuilder<UserInfo>(
                        future: _profileRepository.getUserData(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                                "${snapshot.data?.fullName} ${snapshot.data?.email}");
                          }
                          if (snapshot.hasError) {
                            return const Icon(Icons.warning_amber);
                          }
                          return const CircularProgressIndicator();
                        },
                      ),
                      TextFormField(
                        // obscureText: true,
                        // keyboardType : TextInputType.visiblePassword,
                        controller: _lastnameController,
                        validator: ValidatorService.validateRequired,
                        decoration: const InputDecoration(
                          label: Text("Nom d'utilisateur"),
                          hintText: "email@exemple.com",
                        ),
                      ),
                      TextFormField(
                        controller: _firstnameController,
                        obscureText: true,
                        validator: ValidatorService.validateRequired,
                        decoration: const InputDecoration(
                            label: Text("Mot de passe")),
                      ),
                    ],
                  ),
                ),
              ),
              BlocConsumer<LoginBloc, LoginState>(
                builder: (context, state) {
                  if (state is LoginLoading) {
                    return CircularProgressIndicator();
                  }
                  if (state is LoginSuccess) {
                    return Icon(Icons.verified_outlined);
                  }
                  if (state is LoginFailure) {
                    return Icon(Icons.warning_amber);
                  }

                  return ElevatedButton(
                    onPressed: () => _submitForm(context),
                    child: const Text("Connexion"),
                  );
                },
                listener: (context, state) {
                  if (state is LoginSuccess) {
                    const snackBar = SnackBar(
                      content: Text('Login success'),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
              ),
              ElevatedButton(
                onPressed: () {
                  // Navigator.pop(context);
                  Navigator.of(context).pop();
                },
                child: const Text("Quitter la page"),
              )
            ],
          ),
        ),
      ),
    );
  }

  _submitForm(BuildContext context) {
    // _firstnameController.text = "HELLO";

    if (_formKey.currentState?.validate() == true) {
      print(_firstnameController.text);
      print(_lastnameController.text);

      print("Envoi de l'evenement LoginPressed");
      BlocProvider.of<LoginBloc>(context).add(LoginPressed(
        login: _firstnameController.text,
        password: _lastnameController.text,
      ));
    }
  }
}
