import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:profile_app/app/modules/profile/bloc/profile/profile_bloc.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider_impl.dart';
import 'package:profile_app/app/modules/profile/repository/profile_repository.dart';
import 'package:profile_app/core/di/locator.dart';
import 'package:profile_app/core/utils/validator_service.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _addressController = TextEditingController();
  final _dateController = TextEditingController();
  final _commentController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool _toggleEnabled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profil"),
      ),
      body: SingleChildScrollView(
        child: BlocListener<ProfileBloc, ProfileState>(
          listener: (context, state) {
            if (state is ProfileSuccess) {
              const snackBar = SnackBar(
                content: Text('Enregistré avec succes'),
              );

              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  const Text(
                    "Mes informations personnelles",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                  CachedNetworkImage(
                    imageUrl: "https://picsum.photos/200",
                    placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                    errorWidget: (context, url, error) =>
                    const Icon(Icons.error),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: _nameController,
                    validator: ValidatorService.validateRequired,
                    decoration: const InputDecoration(
                      label: Text("Nom d'utilisateur"),
                      hintText: "Jean Dupond",
                    ),
                  ),
                  TextFormField(
                    controller: _emailController,
                    validator: ValidatorService.validateRequired,
                    decoration: const InputDecoration(
                      label: Text("Adresse E-mail"),
                      hintText: "exemple@email.com",
                    ),
                  ),
                  TextFormField(
                    controller: _addressController,
                    decoration: const InputDecoration(
                      label: Text("Adresse"),
                      hintText: "23 rue Tupin, Lyon",
                    ),
                  ),
                  InkWell(
                    onTap: _selectDate,
                    child: TextFormField(
                      controller: _dateController,
                      enabled: false,
                      decoration: const InputDecoration(
                        label: Text("Date de naissance"),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Mes informations complémentaire",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: _commentController,
                    decoration: const InputDecoration(
                      label: Text("Commentaire"),
                      hintText: "Lorem iptsum ...",
                    ),
                  ),
                  Row(
                    children: [
                      Switch(
                        // This bool value toggles the switch.
                        value: _toggleEnabled,
                        activeColor: Colors.red,
                        onChanged: (bool value) {
                          // This is called when the user toggles the switch.
                          setState(() {
                            _toggleEnabled = value;
                          });
                        },
                      ),
                      const Text("Accepter les conditions d'utilisation"),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    onPressed: () => _submitForm(context),
                    child: const Text("Enregistrer"),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _selectDate() async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
    );
    if (picked != null) {
      _dateController.text = picked.toString();
    }
  }

  _submitForm(BuildContext context) async {
    if (_formKey.currentState?.validate() == true) {
      print('test');
      BlocProvider.of<ProfileBloc>(context).add(
        SavePressed(
          userInfo: UserInfo(
            fullName: _nameController.text,
            email: _emailController.text,
            imageUrl: "",
          ),
        ),
      );
    }
  }
}
