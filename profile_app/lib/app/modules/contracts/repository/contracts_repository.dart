import 'package:profile_app/app/modules/contracts/data/local/contracts_local_provider.dart';
import 'package:profile_app/app/modules/contracts/data/models/contracts_models.dart';
import 'package:profile_app/app/modules/contracts/data/remote/contracts_remote_provider.dart';
import 'package:profile_app/core/di/locator.dart';

abstract class ContractsRepository {

  Future<List<Contract>> getAllContracts();
}

class ContractsRepositoryImpl extends ContractsRepository {

  final ContractsRemoteProvider _contractsRemoteProvider = locator.get();
  final ContractsLocalProvider _contractsLocalProvider = locator.get();

  @override
  Future<List<Contract>> getAllContracts() async {
    await _contractsLocalProvider.init();
    try{
      // Si on arrive bien a recupérer depuis internet
      final remoteContracts = await _contractsRemoteProvider.retrieve();
      // Mettre a jour le cache
      await _contractsLocalProvider.storeAll(remoteContracts);
      return remoteContracts;
    }catch(e){
      // essayer de recuperer depuis le cache
      final cacheContracts = await _contractsLocalProvider.fetchAll();
      return cacheContracts ?? [];
    }
  }

}