import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:profile_app/app/modules/contracts/data/models/contracts_models.dart';
import 'package:profile_app/app/modules/contracts/repository/contracts_repository.dart';
import 'package:profile_app/core/di/locator.dart';

part 'contracts_event.dart';
part 'contracts_state.dart';

class ContractsBloc extends Bloc<ContractsEvent, ContractsState> {

  final ContractsRepository _contractsRepository = locator.get();

  ContractsBloc() : super(ContractsInitial()) {
    on<LoadAllContracts>((event, emit) async {

      emit(ContractsLoading());
      try{
        final contracts = await _contractsRepository.getAllContracts();
        emit(ContractsLoaded(contracts));
      } catch(e){
        print("Contracts error ");
        print(e);
        emit(ContractsError(e));
      }
    });
  }
}
