part of 'contracts_bloc.dart';

@immutable
abstract class ContractsState {}

class ContractsInitial extends ContractsState {}
class ContractsLoading extends ContractsState {}
class ContractsLoaded extends ContractsState {
  final List<Contract> contracts;

  ContractsLoaded(this.contracts);
}
class ContractsError extends ContractsState {
  final dynamic error;

  ContractsError(this.error);
}
