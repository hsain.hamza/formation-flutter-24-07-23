import 'package:profile_app/app/modules/contracts/data/models/contracts_models.dart';
import 'package:profile_app/app/modules/contracts/data/remote/api/contracts_rest_client.dart';
import 'package:profile_app/core/http/http_dio_helper.dart';

abstract class ContractsRemoteProvider{
  Future<List<Contract>> retrieve();
}

class ContractsRemoteProviderImpl extends ContractsRemoteProvider {

  final _contractsRestClient = ContractsRestClient(HttpDioHelper.clientHttp);

  @override
  Future<List<Contract>> retrieve() {
    return _contractsRestClient.getAll();
  }

}