import 'package:dio/dio.dart';
import 'package:profile_app/app/modules/contracts/data/models/contracts_models.dart';
import 'package:retrofit/retrofit.dart';

part 'contracts_rest_client.g.dart';

@RestApi()
abstract class ContractsRestClient {
  factory ContractsRestClient(Dio dio, {String baseUrl}) = _ContractsRestClient;

  @GET("/api/contracts")
  Future<List<Contract>> getAll();
}