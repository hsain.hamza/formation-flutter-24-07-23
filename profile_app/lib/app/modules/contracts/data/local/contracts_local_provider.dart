import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:profile_app/app/modules/contracts/data/models/contracts_models.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

abstract class ContractsLocalProvider {
  Future init();
  Future<List<Contract>?> fetchAll();
  Future storeAll(List<Contract> contracts);
}

class ContractsDbProvider extends ContractsLocalProvider {
  late Database _db;
  late StoreRef<int, Map<String, dynamic>> _contractsStore;


  @override
  Future init() async {
    final documentsPath = await getApplicationDocumentsDirectory();
    final dbPath = join(documentsPath.path, "profile.db");
    _db = await databaseFactoryIo.openDatabase(dbPath);
    _contractsStore = intMapStoreFactory.store("contractsStore");
  }

  @override
  Future<List<Contract>?> fetchAll() async {
    final rawData = await _contractsStore.find(_db);
    if(rawData.isNotEmpty){
      return rawData.map((e) => Contract.fromJson(e.value)).toList();
    }
    return null;
  }

  @override
  Future storeAll(List<Contract> contracts) async{
      await _db.transaction((transaction) async {
        await _contractsStore.delete(transaction);
        for(var contract in contracts){
          await _contractsStore.add(transaction, contract.toJson());
        }
      });
  }



}