// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contracts_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Contract _$ContractFromJson(Map<String, dynamic> json) => Contract(
      contractNumber: json['contract_number'] as String?,
      picture: json['picture'] as String?,
      holder: json['holder'] as String?,
      email: json['email'] as String?,
      phone: json['phone'] as String?,
      address: json['address'] as String?,
      description: json['description'] as String?,
      expire: json['expire'] == null
          ? null
          : DateTime.parse(json['expire'] as String),
    );

Map<String, dynamic> _$ContractToJson(Contract instance) => <String, dynamic>{
      'contract_number': instance.contractNumber,
      'picture': instance.picture,
      'holder': instance.holder,
      'email': instance.email,
      'phone': instance.phone,
      'address': instance.address,
      'description': instance.description,
      'expire': instance.expire?.toIso8601String(),
    };
