import 'package:json_annotation/json_annotation.dart';

part "contracts_models.g.dart";

@JsonSerializable()
class Contract {
  @JsonKey(name: "contract_number")
  final String? contractNumber;
  final String? picture;
  final String? holder;
  final String? email;
  final String? phone;
  final String? address;
  final String? description;
  final DateTime? expire;

  Contract({
    this.contractNumber,
    this.picture,
    this.holder,
    this.email,
    this.phone,
    this.address,
    this.description,
    this.expire,
  });

  factory Contract.fromJson(Map<String, dynamic> json) => _$ContractFromJson(json);

  Map<String, dynamic> toJson() => _$ContractToJson(this);


}
