import 'package:profile_app/app/modules/profile/data/local/profile_local_provider.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider_impl.dart';
import 'package:profile_app/core/di/locator.dart';

abstract class ProfileRepository {
  Future<UserInfo> getUserData();

  Future insertUserData(UserInfo user);

  Future<UserInfo> login(String login, String password);
}

class ProfileRepositoryImpl extends ProfileRepository {
  final ProfileLocalProvider _profileLocalProvider = locator.get();
  final ProfileRemoteProvider _profileRemoteProvider = locator.get();

  @override
  Future<UserInfo> getUserData() async {
    await _profileLocalProvider.init();
    final localUserData = await _profileLocalProvider.fetch();

    if (localUserData != null) {
      return localUserData;
    }

    final remoteUserData = await _profileRemoteProvider.retrieve();
    await _profileLocalProvider.store(remoteUserData);
    return remoteUserData;
  }

  @override
  Future insertUserData(UserInfo user) {
    return _profileRemoteProvider.insert(user);
  }

  @override
  Future<UserInfo> login(String login, String password) {
    return Future.delayed(const Duration(seconds: 2)).then((value) => UserInfo(
          fullName: "Antoine",
          email: "test@email.com",
          imageUrl: "",
        ));
  }
}
