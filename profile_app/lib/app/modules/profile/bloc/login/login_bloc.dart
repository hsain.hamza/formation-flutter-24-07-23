import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/repository/profile_repository.dart';
import 'package:profile_app/core/di/locator.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {

  final ProfileRepository _profileRepository = locator.get();
  LoginBloc() : super(LoginInitial()) {

    on<LoginPressed>((event, emit) async {
      print("Reception de l'evenement LoginPressed");

      emit(LoginLoading());
      print("LoginLoading");

      try{
        final user = await _profileRepository.login(event.login, event.password);
        print("LoginSuccess");

        emit(LoginSuccess(user));
      } catch(e){
        emit(LoginFailure(e));
        print("LoginFailure");

      }
    });
  }
}
