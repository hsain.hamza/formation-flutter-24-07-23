part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}
class LoginLoading extends LoginState {}
class LoginSuccess extends LoginState {
  final UserInfo userInfo;

  LoginSuccess(this.userInfo);
}
class LoginFailure extends LoginState {
  final dynamic error;
  LoginFailure(this.error);
}
