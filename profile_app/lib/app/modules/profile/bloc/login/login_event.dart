part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginPressed extends LoginEvent {
  final String login;
  final String password;

  LoginPressed({
    required this.login,
    required this.password,
  });
}
