import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/repository/profile_repository.dart';
import 'package:profile_app/core/di/locator.dart';

part 'user_info_event.dart';

part 'user_info_state.dart';

class UserInfoBloc extends Bloc<UserInfoEvent, UserInfoState> {
  final ProfileRepository _profileRepository = locator.get();

  UserInfoBloc() : super(UserInfoInitial()) {
    on<UserInfoLoadingData>((event, emit) async {
      emit(UserInfoLoading());

      try {
        final user = await _profileRepository.getUserData();
        emit(UserInfoSuccess(user));
      } catch (e) {
        emit(UserInfoFailure(e));
      }
    });
  }
}
