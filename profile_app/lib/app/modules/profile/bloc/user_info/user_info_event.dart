part of 'user_info_bloc.dart';

@immutable
abstract class UserInfoEvent {}

class UserInfoLoadingData extends UserInfoEvent {
  UserInfoLoadingData();
}