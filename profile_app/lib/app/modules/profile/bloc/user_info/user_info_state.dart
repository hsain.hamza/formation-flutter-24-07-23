part of 'user_info_bloc.dart';

@immutable
abstract class UserInfoState {}

class UserInfoInitial extends UserInfoState {}
class UserInfoLoading extends UserInfoState {}
class UserInfoSuccess extends UserInfoState {
  final UserInfo userInfo;

  UserInfoSuccess(this.userInfo);
}
class UserInfoFailure extends UserInfoState {
  final dynamic error;
  UserInfoFailure(this.error);
}