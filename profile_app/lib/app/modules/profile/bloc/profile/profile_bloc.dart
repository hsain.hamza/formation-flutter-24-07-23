import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/repository/profile_repository.dart';
import 'package:profile_app/core/di/locator.dart';

part 'profile_event.dart';

part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileRepository _profileRepository = locator.get();

  ProfileBloc() : super(ProfileInitial()) {
    on<SavePressed>((event, emit) async {
      emit(ProfileLoading());

      try {

        await _profileRepository.insertUserData(event.userInfo);
        print('success');
        emit(ProfileSuccess());
      } catch (e) {
        print('error');
        emit(ProfileFailure(e));
      }
    });
  }
}
