part of 'profile_bloc.dart';

@immutable
abstract class ProfileEvent {}

class SavePressed extends ProfileEvent {
  final UserInfo userInfo;

  SavePressed({
    required this.userInfo,
  });
}