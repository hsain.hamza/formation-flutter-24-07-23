import 'package:dio/dio.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:retrofit/retrofit.dart';

part 'profile_rest_client.g.dart';

@RestApi()
abstract class ProfileRestClient {
  factory ProfileRestClient(Dio dio, {String baseUrl}) = _ProfileRestClient;

  @GET("/api/userinfo")
  Future<UserInfo> getUser();

  @POST("/api/userinfo")
  Future postUser(@Body() UserInfo data);
}