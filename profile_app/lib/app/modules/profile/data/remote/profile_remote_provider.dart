import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';

abstract class ProfileRemoteProvider{
  Future<UserInfo> retrieve();
  Future insert(UserInfo user);
}