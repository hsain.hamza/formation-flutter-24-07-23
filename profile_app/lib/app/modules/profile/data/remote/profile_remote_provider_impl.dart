import 'dart:convert';

import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:profile_app/app/modules/profile/data/remote/api/profile_rest_client.dart';
import 'package:profile_app/app/modules/profile/data/remote/profile_remote_provider.dart';
import 'package:profile_app/core/http/http_dio_helper.dart';

class ProfileRemoteProviderImpl extends ProfileRemoteProvider {

  final _profileRestClient = ProfileRestClient(HttpDioHelper.clientHttp);

  @override
  Future<UserInfo> retrieve()  {
    return _profileRestClient.getUser();
  }

  @override
  Future insert(UserInfo user) {
    return _profileRestClient.postUser(user);
  }



}
