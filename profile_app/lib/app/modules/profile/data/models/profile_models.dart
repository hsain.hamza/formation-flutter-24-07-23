import 'package:json_annotation/json_annotation.dart';

part 'profile_models.g.dart';

@JsonSerializable()
class UserInfo {
  @JsonKey(name: "full_name")
  final String? fullName;
  final String? email;
  @JsonKey(name: "image_url")
  final String? imageUrl;

  UserInfo({
    required this.fullName,
    required this.email,
    required this.imageUrl,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) => _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);

}
