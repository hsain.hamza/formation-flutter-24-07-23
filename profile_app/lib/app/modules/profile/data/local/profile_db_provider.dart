import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:profile_app/app/modules/profile/data/local/profile_local_provider.dart';
import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class ProfileDbProvider extends ProfileLocalProvider {

  late Database _db;
  late StoreRef<String, Map<String, dynamic>> _userStore;
  // late StoreRef<int, Map<String, dynamic>> _itemsStore;

  @override
  Future init() async {
    final documentsPath = await getApplicationDocumentsDirectory();
    final dbPath = join(documentsPath.path, "profile.db");
    _db = await databaseFactoryIo.openDatabase(dbPath);
    _userStore = stringMapStoreFactory.store("userStore");
    // _itemsStore = intMapStoreFactory.store("itemsStore");
  }

  @override
  Future store(UserInfo data) async {

    //await _itemsStore.add(_db, data.toJson());
    //await _itemsStore.add(_db, data.toJson());
    // await _itemsStore.find(_db,);

    await _userStore.record("user_informations").put(_db, data.toJson());
  }

  @override
  Future<UserInfo?> fetch() async{
    final rawData = await _userStore.record("user_informations").get(_db);
    if(rawData != null){
      return UserInfo.fromJson(rawData);
    }
    return null;
  }




}