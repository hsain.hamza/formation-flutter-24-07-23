import 'dart:convert';

import 'package:profile_app/app/modules/profile/data/models/profile_models.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ProfileLocalProvider {
  Future init();

  Future store(UserInfo data);

  Future<UserInfo?> fetch();
}

class ProfileLocalProviderImpl extends ProfileLocalProvider {
  SharedPreferences? _prefs;

  @override
  Future init() async {
    _prefs ??= await SharedPreferences.getInstance();
  }

  @override
  Future<UserInfo?> fetch() async {
    final String? userData = _prefs?.getString('userData');
    if (userData != null) {
      return UserInfo.fromJson(jsonDecode(userData));
    }

    return null;
  }

  @override
  Future store(UserInfo data) async {
    await _prefs?.setString('userData', jsonEncode(data.toJson()));
  }
}
